import { combineReducers } from 'redux';

import repositories from './repositoriesReducer';
import user from './userReducer';
import pages from './pagesReducer';
import responseStatus from './responseStatusReducer';

const rootReducer = combineReducers({
	repositories,
	user,
	pages,
	responseStatus
});

export default rootReducer;