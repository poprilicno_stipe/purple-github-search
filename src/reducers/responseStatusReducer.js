import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function responseStatusReducer(state = initialState.responseStatus, action) {
    switch (action.type) {
        case types.GRAB_REPOSITORIES_STATUS: {
            return action.responseStatus;
        }
        default:
            return state;
    }
}