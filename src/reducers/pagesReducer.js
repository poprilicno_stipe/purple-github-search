import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function pagesReducer(state = initialState.pages, action) {
    switch (action.type) {
        case types.GRAB_PAGES_SUCCESS: {
            return action.pages;
        }
        default:
            return state;
    }
}