import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function repositoriesReducer(state = initialState.repositories, action) {
    switch (action.type) {
        case types.GRAB_REPOSITORIES_SUCCESS: {
            return action.repositories;
        }
        default:
            return state;
    }
}