import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as repositoriesAction from '../../actions/repositoriesAction';
import Paging from '../pagingComponent/paging';


class PagingContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pages: {}
        };
        this.changePage = this.changePage.bind(this);
    }

    changePage(link) {
        this.props.actions.loadPagedRepositories(link);
    }

    render() {
        let pages = this.props.pages;
        return (
            <nav>
                {/* turn pages object into an array, map it, render all available paging buttons  */}
                {Object.keys(pages).map((page, index) => <Paging changePage={this.changePage} key={index} link={pages[page]} page={page} />)}
            </nav>
        );
    }
}

PagingContainer.propTypes = {
    repositories: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    pages: PropTypes.object.isRequired
};

// STORE
function mapStateToProps(state) {
    return {
        repositories: state.repositories,
        pages: state.pages
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(repositoriesAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PagingContainer);
