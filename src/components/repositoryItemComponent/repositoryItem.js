import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, css } from 'aphrodite';


const RepositoryItem = ({ repository }) => {
    return (
        <article className={css(styles.repositoryArticle)}>
            <header>
                <a className={css(styles.repositoryLink)} href={repository.html_url}>
                    <h4 className={css(styles.repositoryTitle)}>{repository.full_name}</h4>
                </a>
            </header>
            <section>
                <p><span className={css(styles.repositoryInfoTitle)}>
                    <svg className={css(styles.repositoryInfoSvg)} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="align-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M12.83 352h262.34A12.82 12.82 0 0 0 288 339.17v-38.34A12.82 12.82 0 0 0 275.17 288H12.83A12.82 12.82 0 0 0 0 300.83v38.34A12.82 12.82 0 0 0 12.83 352zm0-256h262.34A12.82 12.82 0 0 0 288 83.17V44.83A12.82 12.82 0 0 0 275.17 32H12.83A12.82 12.82 0 0 0 0 44.83v38.34A12.82 12.82 0 0 0 12.83 96zM432 160H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0 256H16a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z">
                        </path>
                    </svg>
                    {/* description text */}
                    Description:</span> {repository.description}
                </p>
                <p><span className={css(styles.repositoryInfoTitle)}>
                    <svg className={css(styles.repositoryInfoSvg)} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="code" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512">
                        <path fill="currentColor" d="M278.9 511.5l-61-17.7c-6.4-1.8-10-8.5-8.2-14.9L346.2 8.7c1.8-6.4 8.5-10 14.9-8.2l61 17.7c6.4 1.8 10 8.5 8.2 14.9L293.8 503.3c-1.9 6.4-8.5 10.1-14.9 8.2zm-114-112.2l43.5-46.4c4.6-4.9 4.3-12.7-.8-17.2L117 256l90.6-79.7c5.1-4.5 5.5-12.3.8-17.2l-43.5-46.4c-4.5-4.8-12.1-5.1-17-.5L3.8 247.2c-5.1 4.7-5.1 12.8 0 17.5l144.1 135.1c4.9 4.6 12.5 4.4 17-.5zm327.2.6l144.1-135.1c5.1-4.7 5.1-12.8 0-17.5L492.1 112.1c-4.8-4.5-12.4-4.3-17 .5L431.6 159c-4.6 4.9-4.3 12.7.8 17.2L523 256l-90.6 79.7c-5.1 4.5-5.5 12.3-.8 17.2l43.5 46.4c4.5 4.9 12.1 5.1 17 .6z">
                        </path>
                    </svg>
                    {/* language text */}
                    Language:</span> {repository.language}
                </p>
                <p><span className={css(styles.repositoryInfoTitle)}>
                    <svg className={css(styles.repositoryInfoSvg)} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z">
                        </path>
                    </svg>
                    {/* update text */}
                    Updated at:</span> <time>{repository.updated_at}</time>
                </p>
                <p><span className={css(styles.repositoryInfoTitle)}>
                    <svg className={css(styles.repositoryInfoSvg)} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                        <path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z">
                        </path>
                    </svg>
                    {/* created text */}
                    Created at:</span> <time>{repository.created_at}</time>
                </p>
                <p><span className={css(styles.repositoryInfoTitle)}>
                    <svg className={css(styles.repositoryInfoSvg)} aria-hidden="true" focusable="false" data-prefix="far" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                        <path fill="currentColor" d="M528.1 171.5L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6zM388.6 312.3l23.7 138.4L288 385.4l-124.3 65.3 23.7-138.4-100.6-98 139-20.2 62.2-126 62.2 126 139 20.2-100.6 98z">
                        </path>
                    </svg>
                    {/* stars */}
                    Stargazers:</span> {repository.stargazers_count}
                </p>
            </section>
            <footer className={css(styles.repositoryFooter)}>
                <svg className={css(styles.repositoryInfoSvg)} aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z">
                    </path>
                </svg>
                Author:<a className={css(styles.repositoryTitle)} href={repository.owner.html_url}> {repository.owner.login} </a>
            </footer>
        </article>
    );
};

RepositoryItem.propTypes = {
    repository: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
    repositoryArticle: {
        maxWidth:"80rem",
        margin: "1rem auto",
        padding: "0 1rem 0 1rem",
        color: "#8a8d93",
        background: "#2d3035",
        ':hover': {
            color: "#bfc1c4",
            backgroundColor: "#34373d",
            transition: "all 0.3s ease"
        },
    },
    repositoryTitle: {
        margin: "0",
        padding: "1rem 0",
        textAlign: "center",
        textDecoration: "none",
        color: "#864DD9",
        ':hover': {
            color: "#CF53F9",
            textDecoration: "underline",
            transition: "all 0.3s ease",
        },
    },
    repositoryLink: {
        textDecoration: "none",
        ':hover': {
            textDecoration: "underline",
        },
    },
    repositoryInfoTitle: {
        color: "#bfc1c4",
        ':hover': {
            color: "#CF53F9",
            transition: "all 0.3s ease",
        },
    },
    repositoryInfoSvg: {
        height: "1rem",
        width: "2rem"
    },
    repositoryFooter: {
        padding: "1rem 0",
        textAlign: "center"
    }
});

export default RepositoryItem;