import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import { Provider } from 'react-redux';

import configureStore from '../store/confgureStore';
import { loadRepositories } from '../actions/repositoriesAction';
import RepositoresContainer from './repositoriesContainerComponent/RepositoriesContainer';
import SearchContainer from '../components/searchContainerComponent/searchContainer';
import PagingContainer from './pagingContainerComponent/pagingContainer';


let currentValue;
function handleChange() {
  let previousValue = currentValue;
  currentValue = store.getState().user;

  if (previousValue !== currentValue) {
    store.dispatch(loadRepositories(store.getState().user));
  }
}


const store = configureStore();
store.dispatch(loadRepositories(store.getState().user));
store.subscribe(handleChange);

class App extends React.Component {
  render() {
    return (
      <div>
        <header className={css(styles.mainHeader)}>
          <h1>
            Purple Github Search
          </h1>
          <p className={css(styles.subtitle)}>Just a simple search app made with React.js, Redux, Axios</p>
        </header>
        <main>
          <Provider store={store}>
            <SearchContainer />
            <RepositoresContainer />
            <PagingContainer />
          </Provider>
        </main>
      </div>
    );
  };
}

const styles = StyleSheet.create({
  mainHeader: {
    margin: "0",
    textAlign: "center",
    color: "#864DD9",
  },
  subtitle: {
    fontSize: "0.875rem",
    fontWeight: "700",
    color: "#8a8d93",
  }
});

export default App;
