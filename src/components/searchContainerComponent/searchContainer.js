import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as userActions from '../../actions/userAction';
import Search from '../searchComponent/search';


class SearchContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: ""
		};
		this.updateSearch = this.updateSearch.bind(this);
		this.saveSearch = this.saveSearch.bind(this);
	}

	updateSearch(e) {
		let user = e.target.value;
		return this.setState({ user: user });
	}

	saveSearch(e) {
		e.preventDefault();
		this.props.actions.grabUser(this.state.user);
	}

	render() {
		return (
			<section>
				<Search
					user={this.state.user}
					onChange={this.updateSearch}
					onSave={this.saveSearch}
				/>
			</section>
		);
	}
}

SearchContainer.propTypes = {
	user: PropTypes.string.isRequired,
	actions: PropTypes.object.isRequired
};

// STORE
function mapStateToProps(state) {
	return {
		user: state.user
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(userActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);
