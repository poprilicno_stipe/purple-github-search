import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, css } from 'aphrodite';


const Search = ({ user, onChange, onSave }) => {
    return (
        <form className={css(styles.searchForm)} onSubmit={onSave}>
            <input className={css(styles.searchInput)} type="text" placeholder="Github username" value={user} onChange={onChange} />
        </form>
    );
};

Search.propTypes = {
    user: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    searchForm: {
        padding: "1rem",
        height: "3rem",
        textAlign: "center",
    },
    searchInput: {
        width: "23rem",
        padding: "0.25rem",
        border: "0",
        outline: "none",
        borderBottom: "1px solid #d8d8d8",
        textAlign: "center",
        fontSize: "1.25rem",
        color: "#8a8d93",
        ':hover': {
            color:"#bfc1c4",
            borderColor: "#864DD9",
        },
        ':focus': {
            color:"#bfc1c4",
            fontSize: "1.8rem",
            borderColor: "#CF53F9",
            transition: "all 0.3s ease"
        }
    }
});

export default Search;