import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, css } from 'aphrodite';


const Paging = ({ link, page, changePage }) => {
    return (
        <button className={css(styles.pagingButton)} onClick={() => changePage(link)}>{page}</button>
    );
};


Paging.propTypes = {
    link: PropTypes.string.isRequired,
    page: PropTypes.string.isRequired,
    changePage: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    pagingButton: {
        padding:"1rem",
        margin: "0.5rem",
        border: "0.25rem solid #864DD9",
        color: "#bfc1c4",
        fontFamily: "'Josefin Sans', sans-serif",
        fontSize: "1.2rem",
        background: "#2d3035",
        ':hover': {
            borderColor: "#CF53F9",
            color: "white",
            backgroundColor: "#34373d",
            transition: "all 0.3s ease",
        }
    }
});

export default Paging;