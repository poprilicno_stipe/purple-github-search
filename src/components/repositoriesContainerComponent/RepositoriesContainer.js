import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import RepositoryItem from '../repositoryItemComponent/repositoryItem';
import * as userActions from '../../actions/userAction';
import ErrorPage from '../errorPageComponent/errorPage';


class RepositoresContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: ""
		};
	}

	repositoryBlock(repository, index) {
		return (
			<div key={index}>
				<RepositoryItem key={repository.id} repository={repository} />
			</div>
		);
	}

	repositoryRenderer() {
		if (this.props.responseStatus[0] === 200) {
			/* sort repositories by stargazers count, map them, render them */
			return this.props.repositories.sort((a, b) => b.stargazers_count - a.stargazers_count).map(this.repositoryBlock);
		} else {
			return <ErrorPage statusArray={this.props.responseStatus}/>
		}
	}

	render() {
		return (
			<section>
				{this.repositoryRenderer()}
			</section>
		);
	}
}

RepositoresContainer.propTypes = {
	repositories: PropTypes.array.isRequired,
	user: PropTypes.string.isRequired,
	actions: PropTypes.object.isRequired,
	responseStatus: PropTypes.array.isRequired
};

// STORE
function mapStateToProps(state) {
	return {
		repositories: state.repositories,
		user: state.user,
		responseStatus: state.responseStatus
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(userActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(RepositoresContainer);
