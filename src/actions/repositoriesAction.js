import axios from 'axios';
import * as types from '../actions/actionTypes';


// parse github api header link string into a usable object
function parseLinks(data) {
    let d = null;
    let linkInfo = null;
    let arrData = data.split("link:")
    data = arrData.length === 2 ? arrData[1] : data;
    let parsed_data = {}
    arrData = data.split(",")
    for (d of arrData) {
        linkInfo = /<([^>]+)>;\s+rel="([^"]+)"/ig.exec(d)
        parsed_data[linkInfo[2]] = linkInfo[1]
    }
    return parsed_data;
}

export function loadRepositoriesSuccess(repositories) {
    return {
        type: types.GRAB_REPOSITORIES_SUCCESS,
        repositories
    };
}

export function loadPagesSuccess(pages) {
    return {
        type: types.GRAB_PAGES_SUCCESS,
        pages
    };
}

export function loadStatus(status, statusText) {
    let responseStatus = [status, statusText];
    return {
        type: types.GRAB_REPOSITORIES_STATUS,
        responseStatus
    };
}

// action that gets repositories based on a search
export function loadRepositories(user) {
    const repositoresPerPage = 10;
    return function (dispatch) {
        return axios
            // get request to github api, repositories desired
            .get(`https://api.github.com/users/${user}/repos?page=1&per_page=${repositoresPerPage}`)
            // request is successful
            .then(repositories => {
                // load repositories into the store, and later the desired component
                dispatch(loadRepositoriesSuccess(repositories.data));
                // load request status and status text into the store, and later the desired component
                dispatch(loadStatus(repositories.status, repositories.statusText));
                // parse and load page links form header into the store, and later the desired component
                if (repositories.headers.link) dispatch(loadPagesSuccess(parseLinks(repositories.headers.link)));
            })
            .catch(error => {
                if (error.response) {
                    // load request error status and error status text into the store, and later the desired component
                    dispatch(loadStatus(error.response.status, error.response.statusText));
                    // load empty page links object to prevent old links and buttons staying in the store/state
                    dispatch(loadPagesSuccess({}));
                }
                throw error;
            });
    };
}

// action that gets repositories based on a page button click
export function loadPagedRepositories(link) {
    return function (dispatch) {
        return axios
            .get(link)
            .then(repositories => {
                // get request to github api, repositories desired
                dispatch(loadRepositoriesSuccess(repositories.data));
                // load request status and status text into the store, and later the desired component
                dispatch(loadStatus(repositories.status, repositories.statusText));
                // parse and load page links form header into the store, and later the desired component
                if (repositories.headers.link) dispatch(loadPagesSuccess(parseLinks(repositories.headers.link)));
            })
            .catch(error => {
                if (error.response) {
                    // load request error status and error status text into the store, and later the desired component
                    dispatch(loadStatus(error.response.status, error.response.statusText));
                    // load empty page links object to prevent old links and buttons staying in the store/state
                    dispatch(loadPagesSuccess({}));
                }
                throw error;
            });
    };
}
