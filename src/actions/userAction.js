import * as types from '../actions/actionTypes';

export function grabUser(user) {
    return {
        type: types.GRAB_USER_SUCCESS,
        user
    };
}
