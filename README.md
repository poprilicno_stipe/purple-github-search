
## About the project

### What is this app able to do:
Simple search form to search for repositories by querying the REST API v3 of GitHub. Display the result in a pagable list, show for an empty result a proper message. Sort the list
by the number of stars. Results should be pagable if there are more than 10 items. List-items should display the full name of the repository incl. a link to the repository to github the programming language of the project, if available, the name of the owner incl. a link to the github owners’ profile, the number of stars.

### Notes
It is styled but there are some minor quirks on smaller screens (viewport width less than 400px, could be easily handled by the media queries), paging buttons are not centered, the app theme is too dark.
There are some parts of the code that are not elegant, and two main actions, that requesting the GithubAPI data, are too similar and not really following DRY principle. They are similar enough to be combined into one function with a bit more conditionals.
Code organization might be a bit too granular for some tastes
There are a few one-line expressions...which might be a bit more difficult to read on first hand. They are commented, though.
For css in js solution I have used Aphrodita which proved to be a poor choice as it's not very expressive or easy to handle. Example: no child selectors, only bunch of classes for each styled element, everything is rendered with "important!" - which is quite annoying IMHO. Not elegant or time-saving at all.
Not sure if I one link inside index.css is enough of a reference for the Font Awesome I have used. Potential copyright and legal issue. Possibly the app would need more clear reference to the icon provider.
Usually, I am more granular and timely with my commits. Professionally, I do tend to push more commits.
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
